var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');


//To add Bus details
router.post('/busDetails', function(req, res){
  
  var busDetails = new req.busDetails({
  BusRouteNumber: req.body.BusRouteNumber,
  BusDescription: req.body.BusDescription,
  FromCity: req.body.FromCity,
  ToCity: req.body.ToCity,
  TotalSeats : req.body.TotalSeats
  });

  busDetails.save(function(err) {
    if (err){
    res.send(err);
    }
    else {            
       res.json("addBus");
    }
  });
});

//To add City details
router.post('/cityDetails', function(req, res){
    
    var cityDetails = new req.cityDetails({
    CityName: req.body.CityName,
    CityDescription: req.body.CityDescription
    });
    cityDetails.save(function(err) {
      if (err){
      res.send(err);
      }
      else {
         res.json("addCity");
      }
    });
  });

//To get all city
router.get('/cityDetails', function(req, res) {
  req.cityDetails.find({}, function(err, city) {
      if (err)
          res.send(err);
      else {
          res.json(city);
      }
  });
});

//To get all Bus details
router.get('/busDetails', function(req, res) {
  req.busDetails.find({}, function(err, bus) {
      if (err)
          res.send(err);
      else {
          res.json(bus);
      }
  });
});

//To get city details by ID
router.get('/cityDetails/:id', function(req, res) {

    let findId = mongoose.Types.ObjectId(req.params.id); 
  req.cityDetails.find({_id: findId }, function(err, city) {
      if (err)
          res.send(err);
      else {
          res.json(city);
      }
  });
});

//To get bus details by ID
router.get('/busDetails/:id', function(req, res) {
    let findId = mongoose.Types.ObjectId(req.params.id); 
  req.busDetails.find({_id: findId }, function(err, bus) {
      if (err)
          res.send(err);
      else {
          res.json(bus);
      }
  });
});

//To get all Booked Bus details
router.get('/bookedBusDetails', function(req, res) {
  req.bookedBusDetails.find({}, function(err, bookedBus) {
      if (err)
          res.send(err);
      else {
          res.json(bookedBus);
      }
  });
});

//To update bus details
router.put('/busDetails', function(req, res) {
    
    let findId = mongoose.Types.ObjectId(req.body._id);
    
  req.busDetails.update ({_id: findId }, {$set: {
    BusRouteNumber: req.body.BusRouteNumber,
    BusDescription: req.body.BusDescription,
    FromCity: req.body.FromCity,
    ToCity: req.body.ToCity,
    TotalSeats : req.body.TotalSeats
 }
},function(err){
  if (err) 
  res.send(err);
  else
  res.json("details updated successfully ");
})
});

//To delete Bus
router.delete('/busDetails/:id', function(req, res) 
{
    let findId = mongoose.Types.ObjectId(req.params.id);
  req.busDetails.remove({_id:findId}, function(err, result) { 
  res.json("deleted successfully");
})
});

//To update city details
router.put('/cityDetails', function(req, res) {

    let findId = mongoose.Types.ObjectId(req.body._id);
  req.cityDetails.update ({_id: findId }, {$set: {
    CityName: req.body.CityName,
    CityDescription: req.body.CityDescription,
 }
},function(err){
  if (err) 
  res.send(err);
  else
  res.json("details updated successfully ");
})
});

//To delete city
router.delete('/cityDetails/:id', function(req, res) 
{
    let findId = mongoose.Types.ObjectId(req.params.id);
  req.cityDetails.remove({_id:findId}, function(err, result) { 
  res.json("deleted successfully");
})
});

module.exports = router;